const express = require('express')
const app = express()
const port = 4000

// mongoose is a package that allows creation of schemas to model our data structure
// also has access to a number of methods for manipulating our databases
const mongoose = require('mongoose')

mongoose.connect('mongodb+srv://admin:admin123@course-booking.flw5f.mongodb.net/B157_to-do?retryWrites=true&w=majority',
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
})

// set notifications for connection success or failure
// connection to the database
// allows us to handle errors when the initial connection is established
let db = mongoose.connection
// if connection error occured, output error message in console
db.on('error', console.error.bind(console,'Connection Error'))
// if connection is successful, output message in console
db.once('open',()=>console.log("We're connected to the cloud database"))

// schemas determine the structure of the documents to be written in the database
// this acts as blueprints to our data
const taskSchema = new mongoose.Schema({
	name:String,
	status:{
		type:String,
		default: 'pending'
	}
})

// model uses schemas and are used to create/instantiate that correspond to the schema
// models must be in singular form and capitalized
// first parameter of the mongoose model method indicates the collection in where it will be stored in the MongoDB collection
// second parameter is used to specify the schema/blueprint of the documents
// using mongoose, the package was programmed well enough that it automatically converts the singular form of the model nam into plural form when creating a collection
const Task=mongoose.model('Task',taskSchema)


// setup for allowing server to handle data from requests
// allows our app to read json data
app.use(express.json())
// allows our app to read data from forms
app.use(express.urlencoded({extended:true}))

// Creating a new Task
/*
	1. add a functionality to check if there is are duplicate tasks
		- if the task already exists in the database, return an error
		- if the task doesn't exist, add it in the database
	2. the task data will be coming from the request's body
	3. create a task object with a "name" field/property

*/

app.post("/tasks", (req, res) => {

	//Check if there are duplicate tasks
	//If there are no matches, the value of result is null
	Task.findOne({name: req.body.name}, (err, result) => {

		//if a document was found and the document's name matches the information sent via the client/Postman
		if (result != null && result.name == req.body.name){
			//Return a message to the client/postman
			return res.send("Duplicate task found");

		//if no document was found	
		} else {

			//Create a new Task and save it to the database
			let newTask = new Task({
				name: req.body.name
			});

			//save method will store the information to the database
			newTask.save((saveErr, savedTask) => {

				//if there are errors in saving
				if(saveErr){
					return console.error(saveErr)

				//no error found while creating the document	
				} else {

					//returns a status code of 201 and sends a message "New Task created."
					return res.status(201).send("New Task created.")
				}

			})

		}

	})

})

// GET request to retrieve all the documents
/*
	1. retrieve all the documents
	2. if an error is encountered, print error
	3. if no errors are found, send success message back to client
*/

app.get('/tasks',(req,res)=>{
	Task.find({},(err,result)=>{
		if(err){
			console.log(err)
		} else{
			return res.status(200).json({
				data:result
			})
		}
	})
})

app.get('/hello',(req,res)=>{
	res.send('Hello World')
})

// Activity
const userSchema = new mongoose.Schema({
	username:String,
	password:String
})

const User=mongoose.model('User',userSchema)

app.post('/signup',(req,res)=>{
	
	User.findOne({username: req.body.username}, (err, result) => {

		
		if (result != null && result.username == req.body.username){
			return res.send("Duplicate user found");	
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			});
	
			newUser.save((saveErr, savedUser) => {
				
				if(saveErr){
					return console.error(saveErr)
				} else {
					return res.send("New User registered.")
				}
			})
		}
	})
})


app.listen(port,()=>{console.log(`Server is running at port ${port}`)})